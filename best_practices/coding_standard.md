# Standard per la scittura del codice

## Controllo sui tipi di dato

PHP offre la direttiva _**declare(strict_types=1)**_ per forzare il controllo stretto sui tipo di parametri e valori
restituiti dalle funzioni. L'utilizzo è caldamente raccomandato; in genere non da problemi a meno che il codice
sia scritto senza attenzione ai tipi di dato utilizzati.

## Dichiarazione del tipo di dato restituito

I tipi dei dati passati come parametri e restituito dalle funzioni andrebbe sempre specificato se non ci siano impedimenti quindi le dichiarazioni
devono essere del tipo:

```php 
function doSomething(SomeType $someVar): void {}
```

```php 
function doSomething(SomeType $someVar): AnotherType {}
```

Oltre a prevenire comportamenti imprevedibili a causa della errata interpretazione dei dati passati, con gli
editor avanzati viene effettuato un controllo già dall'IDE che provvede a segnalare le incongruenze facendo
risparmiare parecchio tempo.
