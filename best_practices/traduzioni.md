# Gestione delle traduzioni

Laravel può gestire le traduzioni utilizzando dei semplici file contenenti le dichiarazioni relative.
Normalmente si utilizzano dei file php o json che restituisce un array contenente i messaggi.
Le funzioni di traduzione ricevono la chiave (*) ed i parametri e restituiscono la stringa compilata con la
sostituzione dei placeholder con i valori dei parametri. 
Il sistema di formattazione utilizzza la notazione che troviamo nella definizione dei parametri delle query quindi
avremo ad esempio 'Errore sul file :filename' e passeremo 'filename' => $filename tra i parametri alla chiamata di
traduzione.

Negli esempi si trova generalmente utilizzata una stringa di descrizione in inglese invece che una chiave; è una
pratica deprecata: per quanto inizialmente eviti rallentamenti nello sviluppo, per la necessità di definire chiavi
e messaggi, offre alcuni svantaggi, tra cui se il messaggio in inglese varia, sarà necessario creare il file d
i traduzioni per l'inglese e mantenere le  corrispondenze

*  &lt;frase inglese vecchia&gt; => &lt;frase inglese nuova&gt;, 
*  &lt;frase inglese vecchia&gt; => &lt;frase in altra lingua>

Questo potrebbe portare al paradosso di avere un testo (usato come chiave) originale con un significato e la
traduzione che ne ha uno molto differente. Per tale motivo è sempre bene utilizzare dei nomi codificati come
chiave.

## struttura del file PHP

Poniamo che il file core.php contenga quanto di seguito:

```php
<?php
return [
    'error' => [
        'internal' => 'Internal server error.',
        'httpresponse' => 'Si è verificato un errore interno.',
        'file' => [
            'saving' => [
                'no_write_permission' => 'Non è stato possibile salvare il file :filename, a causa dei permessi non validi.',
            ]
        ],

    ],
    'exception' => [
        'httpresponse' => '[:errorCode] :message',
    ],
];

```

 potremo generare il testo per un messaggio di errore per file non scrivibile con la chiamata:

```php
__('core.error.file',['filename'=>$outputFilename])
```

## Utilizzo nei moduli

Per utilizzare i testi definiti nei moduli, dovremo fare riferimento al modulo, per cui l'esempio 
precedente diventa qualcosa come:


```php
__('modulenamespace::core.error.file',['filename'=>$outputFilename])
```

## Definizione ed utilizzo di convenzioni

Definendo ed utilizzando correttamente delle convenzioni per le chiavi, le stesse risulteranno generalmente
autoesplicative (quindi fiù facili da ricordare) e sarà possibile creare delle tabella ad uso
interno che riportino tutte le associazioni tra le eccezioni sollevate e:

* modulo interessato
* stato http restituito
* categoria errore
* codice di errore
* file e numero riga in cui l'eccezione viene sollevata
* testo del messaggio
* descrizione estesa del messaggio
* possibili rimedi

potremo inoltre anche creare comandi simili a _**route:list**_, utili agli sviluppatori e mantenere una
base di conoscenze adeguata per il personale dell'assistenza clienti.
