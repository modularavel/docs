<!-- TOC -->
* [Gestione delle eccezioni](#gestione-delle-eccezioni)
  * [Definizione di un formato esteso](#definizione-di-un-formato-esteso)
  * [Classi di eccezione](#classi-di-eccezione)
  * [Implememtazione](#implememtazione)
    * [Classe CoreHttpResponseException](#classe-corehttpresponseexception)
      * [Attributi per i riferimenti al codice](#attributi-per-i-riferimenti-al-codice)
      * [Composizione del codice errore finale e messaggio](#composizione-del-codice-errore-finale-e-messaggio)
      * [Payload esteso](#payload-esteso)
      * [Classi eccezione base](#classi-eccezione-base)
      * [Classi eccezione specializzate](#classi-eccezione-specializzate)
    * [Matrice documentale degli errori](#matrice-documentale-degli-errori)
  * [Convenzioni per la codifica dei codici e dei nomi delle classi](#convenzioni-per-la-codifica-dei-codici-e-dei-nomi-delle-classi)
    * [Tool per la manutenzione delle traduzioni](#tool-per-la-manutenzione-delle-traduzioni)
* [Note: file traduzioni JSON](#note-file-traduzioni-json)
<!-- TOC -->
# Gestione delle eccezioni

## Definizione di un formato esteso

La personalizzazione delle eccezioni tramite la definizione di classi specifiche porta a notevoli
benefici, semplificando la gestione delle segnalazioni e mantenendo omogenei i messaggi prodotti.

Può essere molto utile definire una codifica per le segnalazioni, per poi utilizzarla per indirizzarle
correttamente a chi ne è interessato e per indicare in modo più preciso al softwarista il tipo di errore
riscontrato.

Il nostro messaggio potrebbe quindi contenere:
* codice appplicazione (3 numerici)
* codice modulo (3 numerici)
* codice errore (3 numerici )
* descrizione errore (testo template)
* parametri/valori interessati (valori passati alla descrizione)

I nostri messaggi potrebbero essere quindi formati come qualcosa del tipo:

_**010010001 Conflitto durante il salvataggio del profilo Pippo**_

La segnalazione potrebbe essere fatta in modo da riportare sia il messagio per l'utente finale
che quello per gli sviluppatori, così che il client visualizzi il solo messaggio utente
a meno di essere in una modalità di debug.

Rimane evidente come dover gestire quella mole di dati per ogni segnalazione comporterebbe normalmente
un lavoro lungo e tedioso, con la prospettiva di impazzire per reperire ogni colta i codici corretti
e la necessità di ricercare utilizzi precedenti per non segnalare lo stesso errore in due modi diversi.

Per questo ci vengono incontro le

## Classi di eccezione

La definizione personalizzata delle classi di eccezione permette l'inclusione di tutte quelle operazioni
che effettueremmo _manualmente_ ad ogni sollevamento dell'eccezione.

Questo significa che implementando una corretta gerarchia per coprire tutti gli ambiti dell'applicazione,
avremo un sistema di segnalazioni esteso, granulare e preciso. Le segnalazioni potranno comprendere informazioni
aggiuntive ricevute come parametro da quella sottostante (possibilità di backtrace).

Un ulteriore vantaggio consiste nel poter ridefinire i costruttori in modo che risultino di uso molto più
amichevole, richiedendo i parametri necessari senza ulteriori elaborazioni.

## Implememtazione

Prendiamo in esame alcune delle classi di ecezione personalizzate:

### Classe CoreHttpResponseException

Modules\IRCore\Exceptions\CoreHttpResponseException costituisce la classe di base per le eccezioni in ambito
in ambito API che restituiscono un payload JSON.

```php

<?php
declare(strict_types=1);

namespace Modules\IRCore\Exceptions;

use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class AbstractHttpResponseException extends HttpResponseException
{
    protected int $error_module_code = 1;
    protected int $error_category_code = 2;
    protected int $error_code = 0;
    protected int $status_code = 500;
    protected string $error_key = 'core.error.internal';
    protected string $exception_key = 'core.exception.httpresponse';

    function __construct(array $params = [], \Throwable $parentException = null)
    {
        // compone il codice di errore finale 
        $errorCode = intval(sprintf('%03d%03d%03d',
            $this->error_category_code,
            $this->error_module_code,
            $this->error_code));
            
        // compone il messaggio utente utilizzando la chiave error_key
        $userMessage = __($this->error_key, [$params]);
        
        // compone il messaggio completo di codice e testo
        $messageText = __($this->exception_key, ['errorCode'=>$errorCode, 'message'=>$userMessage]);

        // prepara il payload aggiungendo le informazioni 
        // in modo che i client possano recuperare velocemente i sottocodici
        $payload = [
            'error' => $messageText,
            'module_code' => $this->error_module_code,
            'error_category_code' => $this->error_category_code,
            'error_code' => $this->error_code,
            'status_code' => $this->status_code,
        ];

        // se è stato passata un'eccezione genitore e l'ambiente
        // è configurato per il debug, aggiunge informazioni extra
        // dalla stessa per semplificare il debug con interfacce ajax
        if ( $parentException && config('app.debug') == true) {
            $payload['parent_exception'] = [
                'message' => $parentException->getMessage(),
                'trace' => $parentException->getTrace(),
            ];
        }

        parent::__construct(response()->json($payload,$this->status_code));

    }
}
```
#### Attributi per i riferimenti al codice

```php
    protected int $error_module_code = 1;
    protected int $error_category_code = 2;
    protected int $error_code = 0;
    protected int $status_code = 500;
    protected string $error_key = 'core.error.internal';
    protected string $exception_key = 'core.exception.httpresponse';
```

Gli attributi forniscono i codici e le chiavi relativi all'errore riscontrato; i discendenti
li modificheranno per rispecchiare i valori corretti.

* ```$error_module_code```: codice modulo, ogni modulo avrà un proprio discendente con il codice relativo
 e che sarà utilizzato come predecessore per tuttele eccezioni del modulo.
* ```error_category_code```: codice categoria dell'errore
* ```$error_code```: codice di errore a livello di modulo
* ```$status_code```: codice di stato HTTP da restituire
* ```$error_key```: chiave stringa di errore per l'utente
* ```$exception_key```: chiave stringa di segnalazione eccezione

il codice finale viene composto formattando una stringa contenente i diversi sottocodici:

#### Composizione del codice errore finale e messaggio


```php
 $errorCode = intval(sprintf('%03d%03d%03d',
            $this->error_category_code,
            $this->error_module_code,
            $this->error_code));
        $userMessage = __($this->error_key, [$params]);
        $messageText = __($this->exception_key, ['errorCode'=>$errorCode, 'message'=>$userMessage]);
```

il testo del messaggio utente è composto con:

```php
$userMessage = __($this->error_key, [$params]);
```

quindi riceverà i parametri specializzati passati nell'istruzione throw(); ad esempio per segnalare
un errore su un file, possiamo creare un'eccezione WriteProtectError(string $filename) ed il relativo
template messaggio '**Permesso negato per la scrittura sul file :filename'**

quindi per sollevare l'eccezione utilizzeremo:

```php
throw new WriteProtectError(['filename' => 'filealtrui.txt')
```

senza doverci preoccupare a recuperare i codici interessati.


Il file messaggi contiene:

```php
<?php
return [
    'error' => [
        'internal' => 'Internal server error.',
        'httpresponse' => 'Si è verificato un errore interno.',
        'resource' => [
            'saving' => [
                'trylater' => 'Non è stato possibile salvare le modifiche, riprovare più tardi.',
            ]
        ],

    ],
    'exception' => [
        'httpresponse' => '[:errorCode] :message',
    ],
];

```

quindi la stringa finale risulterà: 
```php
[001001001] Non è stato possibile salvare le modifiche, riprovare più tardi.
```

#### Payload esteso

Per semplificare la vita a chi sviluppa, oltre al messaggio di errore restituiremo i singoli codici, pronti
per essere analizzati e nel caso venga passato un'eccezione padre e sia impostato a debug l'ambiente, 
aggiungiamo ulteriori informazioni dalla stessa (messaggio e trace, eventualmente si può aggiungere altro)

#### Classi eccezione base
Per ogni modulo andremo a creare una classe astratta di base che si occupa semplicemente di impostare
```$error_module_code``` per rispecchiare il proprio codice:

```php
<?php
declare(strict_types=1);

namespace Modules\IRCore\Exceptions;

abstract class ModuleBaseException extends AbstractHttpResponseException
{
    protected int $error_module_code = 0;
}
```
#### Classi eccezione specializzate

Le classi specializzate estenderanno quella di base impostando i codici interessati.

```php
<?php
declare(strict_types=1);

namespace Modules\IRCore\Exceptions;

class ResourceSavingConflictException extends ModuleBaseException
{
    protected int $error_code = 1;
    protected int $status_code = 409;
    protected string $error_key = 'core.error.resource.saving.trylater';

}

```

Siccome restituiamo un codice di stato HTTP, possiamo valutare se sia utile tenerne conto per la gerarchia e/o
nel nome della classe.

### Matrice documentale degli errori

Possiamo quindi creare una tabella per mappare le eccezioni, utile in fase di sviluppo per mantenere coerenza ed
in produzione per l'assistenza tecnica.

| eccezione                       |categoria | modulo     | codice | http status | chiave                              | testo utente                                                     | descrizione                                                                                                 | azione |
|---------------------------------|-----------|-----------|--------|-------------|-------------------------------------|------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|--------|
| ResourceSavingConflictException | 001       | 001       | 001    | 409         | core.error.resource.saving.trylater | Non è stato possibile salvare le modifiche, riprovare più tardi. | la risorsa non è stata salvata a causa di un conflitto fra i dati immessi e le operazioni di altri processi |        |

`
## Convenzioni per la codifica dei codici e dei nomi delle classi

Progettando correttamente il metodo di codifica potremo mantenere delle corrispondenze che aiutino a districarsi
fra i file degli errori. Possiamo pensare ad una struttura standard del tipo:

```php
return [
    'exceptions' => [
        'ResourceSavingConflict' => 'Non è stato possibile salvare le modifiche, riprovare più tardi.',
    ],
]
```

```php
return [
    'exceptions' => [
        'Resource' => [
            'Saving' => [
                'Conflict' => 'Non è stato possibile salvare le modifiche, riprovare più tardi.',
            ],
            'Deleting' => [
                'Notfound' => 'Elemento non trovato per la cancellazione'
            ],
        ],
    ],
]
```

Mantenendo coerenti i nomi delle classi e i percorsi delle stringhe sarà anche più semplice avere una
visione d'insieme che aiuti a definire i parametri comuni e la gerarchia delle classi di
eccezione.

### Tool per la manutenzione delle traduzioni

Una volta stabilite le convenzioni per la nomenclatura di classi e stringhe, sarà possibile valutare
la creazione di un tool per gestire i file delle traduzioni. Eventualmente, una tabella su cui salvare
i testi permetterebbe la gestione via applicazione, quindi anche da personale esterno non esperto nel
trattare sorgenti PHP o JSON. Basterebbe poi una semplice procedura per generare i file aggiornati da
distribuire.

# Note: file traduzioni JSON

Laravel offre la possibilità di utilizzare, come file traduzioni, anche dei file JSON.
La principale differenza rispetto al metodo tradizionale consiste nel dover creare nella
cartella lan un file <lingua>.json (es.json, it.json ecc.). Nella documentazione viene
raccomandato questo metodo per le applicazioni che abbiano un gran numero di traduzioni.
