# Gestione della documentazione

## Tipologie di documentazione

Tipicamente la documentazione è utile a diverse categorie di utilizzatori:

### sviluppatore

* Specifiche
* API
* Routes

### operatore

* Manuale procedure operative

### assistenza utenti

* Manuale degli errori
* Manuale esteso degli errori
* Guida utente

### utente

* Guida utente
* Manuale degli errori

## Metodologie per lo sviluppo e manutenzione della documentazione

### Wiki -> Git
La documentazione potrebbe essere gestita su un Wiki per quanto riguarda lo sviluppo e poi riversata in markdown
utilizzando strumenti come https://github.com/philipashlock/mediawiki-to-markdown

### GitHub -> Web page

Per i progetti ospitati su GitHub è possibile utilizzare lo strumento https://docs.github.com/en/pages/setting-up-a-github-pages-site-with-jekyll
per generare delle pagine HTML statiche dalle pagine del progetto.

## Distribuzione della documentazione

Costruendo l'applicazione come composizione di moduli indipendenti, c'è la necessità di rendere disponibile la documentazione
relativa per ognuno di essi. Se ogni modulo contiene la propria documentazione, secondo chiare convenzioni, potremo costruire
facilmente quella finale per l'intera applicazione tramite automatismi (quindi personalizzata in base ai moduli disponibili)

Per quanto riguarda gli utenti applicativi, l'implementazione migliore sarebbe l'integrazione trasparente, come
finestre popup o hint sui campi delle form, accompagnata dalla disponibilità della guida operativa.

L'assistenza utenti dovrà anche avere a disposizione la documentazione relativa ai codici di errore con relative
procedure di recupero e/o correzione e indicazioni su eventuali escalation della chiamata. Con una corretta
progettazione si potranno costruire strumenti che in base ai codici di errore possano recuperare facilmente tutte
le informazioni tecniche necessarie per risolvere il problema relativo.

Per gli sviluppatori possono essere aggiunti e/o creati strumenti aggiuntivi come report sugli instradamenti e 
swagger per la documentazione delle API.
