# Risorse moduli e Vite #

Utilizzando la struttura a moduli dovremo far caricare e/o ricreare le risorse dei moduli;
normalmente questo viene fatto aggiungendo i percorsi interessati nella sezione input, per 
ottenere qualcosa del tipo:

```js
import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';

export default defineConfig({

    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
                'Modules/ExtendedProfile/Resources/assets/sass/app.scss',
                'Modules/ExtendedProfile/Resources/assets/js/app.js',
            ],

            refresh: [
                ...refreshPaths,
                'app/Http/Livewire/**',
            ],
        }),
        Inspect(),
    ],

});

```

Per ogni asset di ogni modulo, dovremo aggiungere i riferimenti quando aggiungiamo il modulo
e toglierli quando lo rimuoviamo. Per avere una gestione efficace è quindi importante avere
un metodo per poter evitare quelle operazioni.

Specificando il percorso con i caratteri jolly, il tutto funziona correttamente finché si 
utilizzi ```npm run dev``` mentre cercando di effettuare il build per la produzione, non è
in grado di riconoscere i percorsi.

Possiamo allora utilizzare la funzione glob() per ottenere l'elenco da passare al plugin:

```js
import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';
import glob from 'glob'
import Inspect from 'vite-plugin-inspect'

let paths = [
    'resources/css/app.css',
    'resources/js/app.js',

];
// modules resources
glob('Modules/*/Resources/assets/sass/*.scss', function(err, files) {
    if (files.length > 0) {
        for (let i=0; i < files.length; i++) {
            paths.push(files[i]);
        }
    }
})
glob('Modules/*/Resources/assets/js/*.js', function(err, files) {
    if (files.length > 0) {
        for (let i=0; i < files.length; i++) {
            paths.push(files[i])
        }
    }
})

export default defineConfig({

    plugins: [
        laravel({
            input:  paths,
            refresh: true,
        }),
        Inspect(),
    ],

});

```

In questo modo verranno caricate le risorse dell'applicazione e quelle trovate per tutti i moduli installati.

Le risorse potranno quindi essere caricate nelle pagine tramite il comando:

```js
@vite([
    'Modules/ExtendedProfile/Resources/assets/sass/app.scss',
    ])
```
## Note

Nell'esempio vengono utilizzati fogli di stile SCSS; possono essere anche utilizzati dei semplici CSS nel cui caso non
sarà necessaria la conversione per il deploy (quindi non richiede SASS). Ove possibile, l'utilizzo di SCSS è raccomandato
in quanto estende la sintassi di CSS permettendo una gestione più chiara delle gerarchie delle classi.

## Requisiti ##

Devono essere installati i pacchetti 
- 
- sass (per la compilazione dei file scss)
- glob (per recupero elenco file)
