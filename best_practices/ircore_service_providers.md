<!-- TOC -->
* [Service Providers](#service-providers)
  * [Classe di base astratta](#classe-di-base-astratta)
    * [Metodi predefiniti](#metodi-predefiniti)
      * [registerTranslations()](#registertranslations)
      * [registerConfig();](#registerconfig)
      * [registerViews();](#registerviews)
      * [loadMigrationsFrom(module_path($this->moduleName, $this->migrationsPath));](#loadmigrationsfrommodulepaththis-modulename-this-migrationspath)
      * [registerObservers();](#registerobservers)
    * [Le classi derivate](#le-classi-derivate)
      * [IRCoreModuleServiceProvider](#ircoremoduleserviceprovider)
<!-- TOC -->

# Service Providers

## Classe di base astratta

Il modulo IRCore definisce la classe astratta ```phpAbstractModuleServiceProvider ``` che verrà utilizzata come base per tutti i
service provider. I metodi definiti impostano quanto necessario per appoggiarsi alle convenzioni per la struttura dei moduli.

```php
<?php
declare(strict_types=1);

namespace Modules\IRCore\Providers;

use Illuminate\Support\ServiceProvider;

abstract class AbstractModuleServiceProvider extends ServiceProvider
{
    protected string $moduleName = 'Module';
    protected string $moduleNameLower = 'module';
    protected string $migrationsPath = 'Database/Migrations';
    protected string $configPath = 'Config/config.php';
    protected string $defaultLangPath = 'lang/modules/';
    protected string $defaultViewsPath = 'views/modules/';
    protected string $modulesLangPath = 'Resources/lang';
    protected string $moduleViewsPath = 'Resources/views';

    public function boot(): void
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, $this->migrationsPath));
        $this->registerObservers();
    }

    protected function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }

    public function provides(): array
    {
        return [];
    }

    protected function registerConfig(): void
    {
        $this->publishes([
            module_path($this->moduleName, $this->configPath) => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, $this->configPath), $this->moduleNameLower
        );
    }
    protected function registerObservers(): void
    {}

    protected function registerTranslations(): void
    {
        $langPath = resource_path($this->defaultLangPath . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
            $this->loadJsonTranslationsFrom($langPath);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, $this->modulesLangPath), $this->moduleNameLower);
            $this->loadJsonTranslationsFrom(module_path($this->moduleName, $this->modulesLangPath));
        }
    }

    protected function registerViews()
    {
        $viewPath = resource_path($this->defaultViewsPath . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, $this->moduleViewsPath);

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }
}

```

### Metodi predefiniti

Sono predefiniti alcuni metodi per il calcolo dei percorsi

#### registerTranslations()

ricerca le traduzioni da caricare, cerca in /lang o Modules/<modulo>/Resources/lang

#### registerConfig();

registra il file di configurazione del modulo

#### registerViews();

registra il percorso per la ricerca delle viste, in /resources/views/ o Modules/<modulo>/Resources/views

#### loadMigrationsFrom(module_path($this->moduleName, $this->migrationsPath));

carica i file di migrazioni

#### registerObservers();

registra gli observer

### Le classi derivate

Prendiamo in esempio le classi derivate:

#### IRCoreModuleServiceProvider

```php
<?php
declare(strict_types=1);

namespace Modules\IRCore\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\IRCore\Providers\RouteServiceProvider;

class IRCoreModuleServiceProvider extends AbstractModuleServiceProvider
{
    protected string $moduleName = 'IRCore';
    protected string $moduleNameLower = 'ircore';
}
```

Come possiamo vedere, ci basterà impostare correttamente i valori di ```$moduleName``` e ```$moduleNameLower``` per integrare
correttamente la struttura delle risorse del modulo.
