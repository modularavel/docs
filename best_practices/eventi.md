# Eventi e Listener in Laravel

<!-- TOC -->
* [Eventi e Listener in Laravel](#eventi-e-listener-in-laravel)
  * [Eventi associati ai Modelli](#eventi-associati-ai-modelli)
  * [Eventi Custom](#eventi-custom)
    * [Il metodo sbrigativo](#il-metodo-sbrigativo)
    * [Definizione di un evento](#definizione-di-un-evento)
    * [Definizione del listener](#definizione-del-listener)
      * [Eventi accodati](#eventi-accodati)
    * [Attivazione evento](#attivazione-evento)
  * [Note](#note)
<!-- TOC -->


Gli _eventi_ di Laravel forniscono una semplice implementazione del _modello di osservazione_ (observer), 
consentendo di sottoscrivere e ascoltare vari eventi che si verificano all'interno dell'applicazione

Le classi di eventi vengono in genere archiviate nella directory app/Events, mentre i listener 
vengono archiviati in app/Listeners.

Gli eventi rappresentano un ottimo modo per disaccoppiare vari aspetti dell'applicazione, poiché un singolo
evento può avere più listener che non dipendono l'uno dall'altro. Ad esempio è possibile prevedere che ad ogni 
avanzamento di stato di un ordine, venga generato un evento ed aggiungere diversi listener a seconda delle
operazioni aggiuntive (es. inviare email, calcolare una commissione sulla vendita ecc.).

Laravel offre dei pratici meccanismi per la definizione di eventi e listener per l'esecuzione di codice a fronte di evento.
Tramite gli eventi, diversi moduli possono comunicare fra di loro pur rimanendo indipendenti.

L'utilità principale degli eventi si presta 
 * al **tracking dello stato**: gli eventi alimentano un sistema monitor ed efettuano azioni slegate dall'elaborazione *principale*
come inviare un messaggio con la situazione della spedizione aggiornata.
 * alla **gestione del flusso**: il flusso di elaborazione non viene più gestito da un unico processo
che deve attendere il termine di ogni singola operazione per proseguire; ad esempio l'invio di un'email di conferma
non è più eseguito direttamente  ma demandato ad un listener che potrà essere eseguito in parallelo con il resto.
 * **estensione** delle elaborazioni a fronte di un evento, senza la necessità di intervenire sul codice che
effettua l'operazione la transazione.

Utilizzando gli eventi il sistema risulterà assemblato con componenti che effettuano compiti semplici, e
non hanno la necessità di conoscersi ed interagire direttamente tra di loro (es. tramite interfaccia API)
ed in qualsiasi momento possono essere aggiunti o rimossi moduli che comunicano con gli altri tramite eventi
e listener.
 
Infine, gli eventi possono essere gestiti tramite coda.

Laravel fornisce degli eventi standard associati ai modelli  [eventi standard associati ai modelli](#eventi-associati-ai-modelli)   e
le funzionalità per definire ed utilizzare [eventi personalizzati.](#eventi-custom)

## Eventi associati ai Modelli

Laravel permette l'associazione di eventi ai modelli in modo semplice e standardizzato.

Gli eventi standard sono:
 - creating
 - created
 - updating
 - updated
 - deleting
 - deleted
 - replicating
 - retrieved
 - saving
 - saved

l'associazione viene fatta nel metodo boot() del modello tramite i relativi metodi della trait HasEvents.

```php

public static function boot()
{
    parent::boot();

    static::creating(function ($item) {
        Log::info('Creating event call: ' . $item);
    });

    static::created(function ($item) {
        Log::info('Created event call: ' . $item);
    });

    static::updating(function ($item) {
        Log::info('Updating event call: ' . $item);
    });

    static::updated(function ($item) {
        Log::info('Updated event call: ' . $item);
    });

    static::deleting(function ($item) {
            Log::info('Deleting event call: ' . $item);
    });
    
    static::deleted(function ($item) {
        Log::info('Deleted event call: ' . $item);
    });

}

```

in questo caso, ogni operazione CRUD sul modello verrà registrata nel file di log.

Questo comporta che potremo evitare di farcire i controller con registrazioni di log, intercettando le operazioni e provvedendo alle registrazioni.
Ovviamente si presta anche ad azioni più complesse come l'accredito di una provvigione ad un rappresentante ogni volta che un suo cliente effettua un ordine.

L'utilizzo degli eventi permette inoltre  la creazione di moduli aggiuntivi per nuove funzionalità basate su eventi 
di moduli di base.

## Eventi Custom

La classe App\Providers\EventServiceProvider incluso con l'applicazione Laravel offre una posizione
comoda per registrare tutti i listener di eventi dell'applicazione.

### Il metodo sbrigativo
La proprietà listen contiene un array di tutti gli eventi (chiavi) e dei relativi listener 
(valori). È possibile aggiungere tutti gli eventi richiesti dall'applicazione a questo array. 

Per utilizzare eventi ci sono diverse possibilità; il metodo più sbrigatovo è di definire in AppServiceProvider l'elenco di eventi con i relativi
listener. Ad esempio supponiamo di voler definire un evento che a fronte di un like invii un messaggio di congratulazioni all'utente:

```php
<?php
namespace App\Providers;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OrderShipped::class => [
        SendShipmentNotification::class,
    ],
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
```
dovremo anche dichiarare le classi evento e listener _OrderShipped_ e _SendShipmentNotification_ tramite
il singolo comando `php artisan event:generate` (da documentazione, con i moduli sembra non funzionare) o
con i due comandi `php artisan make:event OrderShipped` e `php artisan make:listener SendShipmentNotification --event=PostLiked` 


 - [definire l'evento](#definizione-di-un-evento)
 - [definire uno o più listener](#definizione-del-listener)
 - [aggiungere il codice di segnalazione dell'evento nei metodi interessati](#attivazione-evento)

Ovviamente possiamo limitarci a definire dei listener per eventi già esistenti (ad esempio <code>Illuminate\Auth\Events\Login</code>) per aggiungere delle funzionalità.

### Definizione di un evento

Gli eventi sono definiti tramite classi che costituiscono un wrapper per l'oggetto dell'evento, ad esempio
il nostro evento **OrderShipped** riceve un oggetto **Eloquent ORM**:

```php

namespace App\Events;

use App\Models\Order;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderShipped
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(
        public Order $order,
    ) {}
}
```

il trait ```SerializesModels``` è utilizzato dall'evento per serializzare qualsiasi modello Eloquent se 
l'evento viene serializzato tramite la funzione ```serialize``` di PHP, come quando viene utilizzato un
***queued listener***

### Definizione del listener

Le classi listener rispondono agli eventi tramite il proprio metodo handle() che riceve l'oggetto definito nell'evento

```php
namespace App\Listeners;

use App\Events\OrderShipped;

class SendShipmentNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        // ...
    }

    /**
     * Handle the event.
     */
    public function handle(OrderShipped $event): void
    {
        // Access the order using $event->order...
    }
}

```


#### Eventi accodati

L'accodamento dei listener può essere utile se il listener sta per eseguire un'attività lenta 
come l'invio di un'e-mail o la richiesta HTTP. Prima di utilizzare i listener in coda, assicurarsi 
di configurare la coda e avviare un _queue worker_ nel server o nell'ambiente di sviluppo locale.
Per specificare che un listener deve essere accodato, aggiungere l'interfaccia **ShouldQueue** 
alla classe listener. 

```php
<?php

namespace App\Listeners;

use App\Events\OrderShipped;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendShipmentNotification implements ShouldQueue
{
    // ...
}
```

Ora, quando viene inviato un evento gestito da questo listener, il listener verrà
automaticamente messo in coda dal dispatcher degli eventi utilizzando il sistema di code di Laravel.
Se non vengono generate eccezioni quando il listener viene eseguito dalla coda, il processo in coda
verrà automaticamente eliminato al termine dell'elaborazione.


```php
use App\Events\OrderShipped;
use function Illuminate\Events\queueable;
use Illuminate\Support\Facades\Event;
 
/**
 * Register any other events for your application.
 */
public function boot(): void
{
    Event::listen(queueable(function (OrderShipped $event) {
        // ...
    }));
}

```

potremo inoltre gestire le eccezioni tramite il metodo catch:

```php
use App\Events\OrderShipped;
use function Illuminate\Events\queueable;
use Illuminate\Support\Facades\Event;
use Throwable;
 
Event::listen(queueable(function (OrderShipped $event) {
    // ...
})->catch(function (OrderShipped $event, Throwable $e) {
    // The queued listener failed...
}));

```

### Attivazione evento

L'evento può essere attivato nel codice di controllo:

```php
namespace App\Http\Controllers;

use App\Events\OrderShipped;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class OrderShipmentController extends Controller
{
    /**
     * Ship the given order.
     */
    public function store(Request $request): RedirectResponse
    {
        $order = Order::findOrFail($request->order_id);

        // Order shipment logic...

        OrderShipped::dispatch($order);

        return redirect('/orders');
    }
}

```

## Note

Le opportunità offerte da Laravel non si fermano a quanto descritto, per un ulteriore
approfondimento, vedere i metodi:

- ```dispatchIf($condition, $object)```
- ```dispatchUnless($condition, $object)```
