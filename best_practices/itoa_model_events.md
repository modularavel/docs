<!-- TOC -->
* [Implementazioni specifiche per la gestione eventi sui modelli](#implementazioni-specifiche-per-la-gestione-eventi-sui-modelli)
  * [La classe di estensione base per i modelli](#la-classe-di-estensione-base-per-i-modelli)
    * [La classe di base per l'intero ambiente](#la-classe-di-base-per-lintero-ambiente)
      * [EventAwareModelObserver](#eventawaremodelobserver)
    * [DataHistoryEnabledModelObserver](#datahistoryenabledmodelobserver)
    * [AffiliateRegistered](#affiliateregistered)
    * [AffiliateProfile](#affiliateprofile)
    * [Note](#note)
<!-- TOC -->

# Implementazioni specifiche per la gestione eventi sui modelli

## La classe di estensione base per i modelli

Per implementare gli eventi legati ai modelli utilizzeremo una classe astratta di base che
generalizzi le operazioni di routine quindi per ogni modulo una classe astratta di base per impostare
i parametri comuni entro il modulo, eventuali classi intermedie e finalmente le classi specializzate.


### La classe di base per l'intero ambiente

Il modulo IRCore definisce la classe astratta AbstractHttpResponseException vch

#### EventAwareModelObserver

AbstractEventAwareModelObserver è la classe astratta di base per le eccezioni, qui vengono definiti i valori comuni per
tutti gli eventi

```php

<?php
declare(strict_types=1);

namespace Modules\IRCore\Observers;

abstract class AbstractEventAwareModelObserver
{

}
```

### DataHistoryEnabledModelObserver

DataHistoryEnabledModelObserver costituisce la classe di base per gli eventi di tracciamento delle operazioni
sul db.

```php
<?php
declare(strict_types=1);

namespace Modules\IRCore\Observers;

use Illuminate\Support\Facades\DB;
use Modules\IRCore\Exceptions\ResourceSavingConflictException;
use Modules\IRCore\Models\EventAwareModel;
class DataHistoryEnabledModelObserver extends AbstractEventAwareModelObserver
{

    public const TABLE_HISTORY      = 'data_history';
    public const EVENT_INSERT       = 'INSERT';
    public const EVENT_UPDATE       = 'UPDATE';
    protected const KEY_PASSWORD    = 'password';
    protected const KEY_NEW         = 'new';
    protected const KEY_OLD         = 'old';
    protected const FMT_TIMESTAMP   = 'Y-m-d H:i:s';
    protected const FALLBACK_USER_ID = 1;

    protected function eventType(EventAwareModel $model) :string
    {
        return empty($model->getOriginal()) ? self::EVENT_INSERT: self::EVENT_UPDATE;
    }
    
    public function saved(EventAwareModel $model): void
    {
        $event = $this->eventType($model);
        $timestamp = date(self::FMT_TIMESTAMP);
        $user_id = auth()->user()->id ?? self::FALLBACK_USER_ID;
        $ip = request()->ip();
        $uri = request()->fullUrl();
        $entity = $model->getTable();
        $entity_id = $model->getKey();

        $dirty = $model->getDirty();

        $toSave = [];

        foreach ($dirty as $key => $value) {
            if ($key !== self::KEY_PASSWORD) {
                $toSave[$key] = [
                    self::KEY_NEW => $value,
                    self::KEY_OLD => $model->getOriginal($key) ?? ''
                ];
            } else {
                $toSave[$key] = [
                    self::KEY_NEW => self::KEY_NEW,
                    self::KEY_OLD => self::KEY_OLD
                ];
            }

        }

        $data = json_encode($toSave);

        $saveData = compact('timestamp', 'user_id', 'ip', 'uri', 'event', 'entity', 'entity_id', 'data');

        $this->saveEventData($saveData);

    }

    public function saving(EventAwareModel $model): void
    {
        if (!empty($model->getOriginal('updated_at')) && $model->getAttribute('updated_at') != $model->getOriginal('updated_at')) {
            throw new ResourceSavingConflictException();
        }
    }

    protected function saveEventData($data): void
    {
        DB::table(self::TABLE_HISTORY)->insert($data);
    }

}

```

### AffiliateRegistered

La classe affiliate registered definisce un evento per il modulo ExtendedProfile

```php
<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Events;

use Illuminate\Foundation\Events\Dispatchable;

class AffiliateRegistered
{
    use Dispatchable;

    public function __construct()
    {
    }
}
```

### AffiliateProfile

La classe modello interessata dovrà solo più aggiungere l'avvio dell'osservatore nel proprio metodo ```booted()```

```php
<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Models;

use App\Models\Scopes\Searchable;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\ExtendedProfile\Database\Factories\AffiliateProfileFactory;
use Modules\IRCore\Observers\DataHistoryEnabledModelObserver;
use Spatie\Permission\Traits\HasRoles;

class AffiliateProfile extends \Modules\IRCore\Models\EventAwareModel
{

    ...

    protected static function booted()
    {
        self::observe(DataHistoryEnabledModelObserver::class);
    }
    
    ...

}

```

### Note

Il binding dell'evento può essere impostato anche nel metodo ```boot()``` del service provider. Per ottenere
un sistema elastico (che ci permetta di attivare e disattivare i listener senza dover intervenire sul codice),
possiamo pensare a mantenere un elenco di classi observer nel DB e creare una pagina per gestire un flag
quindi modificare le funzioni boot() o booted() in modo che attivino tutti quelli abilitati. 
