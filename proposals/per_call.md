# da discutere

## Gestione documentazione
vedi documentazione.md

# OBSOLETO #
## utilizzo enum ##

## differenziazione evento per data_history per update o insert  ##

## utilizzo di schema::timestamp() invece di $table-&gt;dateTime('timestamp')  ##

## utilizzo softDelete ##

## migrazione funzionalità core dell'applicazione in IRCore (nome modificabile) ##

## mantenimento di migrazioni e seeder nei moduli interessati ##

## integrazione profilo utenti/clienti ITOA ##

## rename ModelHelper in HasDataTables ##

## modulo laravel datatables  ##
 
https://github.com/yajra/laravel-datatables

##  classi service provider di base AbstractModuleServiceProvider e AbstractRouteServiceProvider

## clonazione progetto su gitlab invece che permesso di accesso ##

Se funziona come github, posso clonare i vostri progetti quindi creare i rami miei e quando faccio il push parte in
automatico la pull request a voi; in tale modo diminuisce il rischio di fare danni.
